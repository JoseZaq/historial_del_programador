```mermaid
 graph TD;
 \ --> receptas;
 recepta --> id((id))
 recepta --> fecha((fecha))
 receptas --> recepta;
 recepta --> nom
 recepta --> categoria
 recepta --> informacio_general
 recepta --> ingredients
 recepta --> preparacio

 informacio_general --> foto
 informacio_general --> enllaç
 informacio_general --> comensals
 informacio_general --> temps
 informacio_general --> dificultat
 informacio_general --> resum
```