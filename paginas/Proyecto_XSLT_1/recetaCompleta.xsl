<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/recepta">  
        <div>
            <xsl:attribute name="class">color_box</xsl:attribute>
            <h1>Receta de <xsl:value-of select="categoria"/></h1>
        </div>
        <xsl:element name="section">
            <xsl:element name="img">
                <xsl:attribute name="class">center</xsl:attribute>
                <xsl:attribute name="src">
                    <xsl:value-of select="informacio_general/foto"/>
                </xsl:attribute>
            </xsl:element>
            <xsl:element name="h2">
                <xsl:value-of select="nom"/>
            </xsl:element>
        </xsl:element>
        <xsl:element name="section">
            <h3>Resumen</h3>
            <xsl:element name="p">
                <xsl:value-of select="informacio_general/resum"/>
            </xsl:element>
        </xsl:element>
        <xsl:element name="section">
            <div>
                <xsl:attribute name="class">resum</xsl:attribute>
                <p> &#128106; <xsl:value-of select="informacio_general/comensals"/></p>
                <p>&#9202; <xsl:value-of select="informacio_general/temps"/> <xsl:value-of select="informacio_general/temps/@unitat"/></p>
                <p>&#129001; &#129000; &#128997; <xsl:value-of select="informacio_general/dificultat"/></p>                
                <p>&#127858; <xsl:value-of select="categoria"/></p>
            </div>
            <div>
                <xsl:attribute name="class">
                    ingredients
                </xsl:attribute>
                <h3>Ingredientes</h3>
                    <nav>
                        <ul>
                            <xsl:for-each select="ingredients/ingredient">
                                <li>
                                    <xsl:value-of select="."/> >> <xsl:value-of select="./@quantitat"/> <xsl:value-of select="./@unitat"/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </nav>
            </div>
            
        </xsl:element>
        <xsl:element name="section">
            <h3>Preparación</h3>
            <xsl:for-each select="preparacio/pas">
                <div>
                    <h4>Paso <xsl:value-of select="@numero"/>: </h4>
                    <p><xsl:value-of select="."/></p>
                </div>
            </xsl:for-each>
            
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>