<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>-->
    <xsl:template match="/">
        <!-- Mostramos todas las cabeceras
        Debe quedar con las misma distribución que en la página original-->
        <xsl:for-each select="receptes/recepta">
            <xsl:element name="article">
                <xsl:attribute name="class">content-recetas</xsl:attribute>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="informacio_general/foto"/>
                    </xsl:attribute>
                </img>
                <h3>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="informacio_general/enllaç"/>
                        </xsl:attribute>
                        <xsl:value-of select="nom"/>
                    </a>
                </h3>
                <p>
                    <xsl:attribute name="class">fecha</xsl:attribute>
                        <xsl:value-of select="@fecha"/>
                </p>
                <p>
                    <xsl:value-of select="informacio_general/resum"/>
                </p>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>